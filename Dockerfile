FROM sonarsource/sonar-scanner-cli:4.6

COPY pipe /
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]