#!/bin/bash

source "$(dirname "$0")/common.sh"

parse_environment_variables() {
  SONAR_HOST_URL=${SONAR_HOST_URL:?'SONAR_HOST_URL variable is missing.'}
  SONAR_TOKEN=${SONAR_TOKEN:?'SONAR_TOKEN variable is missing.'}
  EXTRA_ARGS=${EXTRA_ARGS:=""}
  SONAR_SCANNER_OPTS=${SONAR_SCANNER_OPTS:=""}
  DEBUG=${DEBUG:="false"}
}

parse_environment_variables

ALL_ARGS=${EXTRA_ARGS}

if [[ "${DEBUG}" == "true" ]]; then
  ALL_ARGS=( "-X ${ALL_ARGS}" )
  debug "EXTRA_ARGS: ${EXTRA_ARGS}"
  debug "ALL_ARGS:${ALL_ARGS}"
  debug "SONAR_SCANNER_OPTS: ${SONAR_SCANNER_OPTS}"
fi

sonar-scanner $ALL_ARGS

cp .scannerwork/report-task.txt ${BITBUCKET_PIPE_STORAGE_DIR}/