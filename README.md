# Bitbucket Pipelines Pipe: SonarQube scan

Scan your code with [SonarQube](https://sonarqube.org) to detects Bugs, Vulnerabilities and Code Smells in up to 27 programming languages.

_NOTE: For projects using Maven or Gradle, please execute a respective scanner directly instead of using this pipe._

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: sonarsource/sonarqube-scan:1.0.0
  # variables:
  #   SONAR_HOST_URL: '<string>'  # Optional if set as repository/accout variable
  #   SONAR_TOKEN: '<string>'  # Optional if set as repository/accout variable
  #   EXTRA_ARGS: '<string>'  # Optional
  #   SONAR_SCANNER_OPTS: '<string>'  # Optional
  #   DEBUG: '<boolean>'  # Optional
```

## Variables

| Variable            | Usage                                                                                                                                                                                                |
| ------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| SONAR_HOST_URL (\*) | The public url of your SonarQube instance. It is recommended to use a secure repository or account variable. If so, there is no need to specify this variable in the `bitbucket-pipelines.yml` file. |
| SONAR_TOKEN (\*)    | Your SonarQube access token. It is recommended to use a secure repository or account variable. If so, there is no need to specify this variable in the `bitbucket-pipelines.yml` file.               |
| EXTRA_ARGS          | Extra [analysis parameters](https://redirect.sonarsource.com/doc/analysis-parameters.html)                                                                                                           |
| SONAR_SCANNER_OPTS  | Scanner JVM options (e.g. "-Xmx256m")                                                                                                                                                                |
| DEBUG               | Turn on extra debug information. Default to `false`.                                                                                                                                                 |

_(\*) = required variable._

## Details

This pipe encapsulates the execution of the SonarQube code analyzer in order to detect Bugs, Vulnerabilities and Code Smells. Starting with [Developer edition](https://redirect.sonarsource.com/editions/developer.html), SonarQube can decorate your Pull Requests and report back with code quality information.

## Prerequisites

To run an analysis on your code, you will need the public url of your SonarQube instance, and an access token to it. These two values have to be provided to the scanner through `SONAR_HOST_URL` and `SONAR_TOKEN` variables.

## Examples

Basic example:

```yaml
- pipe: sonarsource/sonarqube-scan:1.0.0
```

A bit more advanced example:

```yaml
- pipe: sonarsource/sonarqube-scan:1.0.0
  variables:
    EXTRA_ARGS: -Dsonar.eslint.reportPaths=\"report.json\"
    SONAR_SCANNER_OPTS: -Xmx512m
    DEBUG: "true"
```

This example reads the report produced by eslint, sets maximum memory to 512MB, and enables verbose output.

## Support

To get help with this pipe, or to report issues or feature requests, please get in touch on [our community forum](https://community.sonarsource.com/tags/c/help/sq/bitbucketcloud).

If you are reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
